<?php 
	include("db.php");
	include "Parsedown.php";
    $Parsedown = new Parsedown();
?>
<!DOCTYPE HTML>
<html>
<head>

<meta charset="utf-8">
<link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400italic,700|Open+Sans+Condensed:300,700" rel="stylesheet">


<link rel="stylesheet" href="assets/css/main.css" />
<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
     <script src="http://code.jquery.com/jquery-latest.min.js//ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/jquery-ui.min.js"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="assets/js/main.js"></script>
</head>
<body class="left-sidebar">
<div id="wrapper">
  <div id="content" class="mobileUI-main-content">
    <div id="content-inner">
      
	  
	  <?php
	  if($_GET['id'] != "")
	  {
	  	$id = $_GET['id'];
	  	if($id == "overzicht")
		{	
			echo "<title>Overzicht</title>";
			$query = "SELECT * FROM Article order by year, month, day";
			$result = mysql_query($query);
			if(mysql_num_rows($result) > 0)
			{
				while($row = mysql_fetch_assoc($result))
				{
					echo "<h2><a href='index.php?id=";
					echo $row['id'];
					echo "'>";
					echo $row['title'];
					echo "</a></h2>";
					echo "<hr>";
				}
			}	
		}
		elseif($id=="taken")
		{
			echo "<title>Taken</title>";
			$query = "SELECT * FROM Article where type='taken' order by year, month, day";
			$result = mysql_query($query);
			if(mysql_num_rows($result) > 0)
			{
				while($row = mysql_fetch_assoc($result))
				{
					echo "<h2><a href='index.php?id=";
					echo $row['id'];
					echo "'>";
					echo $row['title'];
					echo "</a></h2>";
					echo "</hr>";
				}
			}	
		}
		elseif($id=="stage")
		{
			echo "<title>stage Verslagen</title>";
			$query = "SELECT * FROM Article where type='stage' order by year, month, day";
			$result = mysql_query($query);
			if(mysql_num_rows($result) > 0)
			{
				while($row = mysql_fetch_assoc($result))
				{
					echo "<h2><a href='index.php?id=";
					echo $row['id'];
					echo "'>";
					echo $row['title'];
					echo "</a></h2>";
					echo "<hr>";
				}
			}
		}
		else
		{
			$query = "SELECT * FROM Article WHERE id='$id';";
			$result = mysql_query($query);
			if(mysql_num_rows($result) > 0)
			{
				while($row = mysql_fetch_assoc($result))
				{
					echo "<title>";
					echo $row['title'];
					echo "</title>";
					echo '<article class="is-post is-post-excerpt"><header><h2><a href="#">';
					echo $row ['title'];
					echo '</a></h2><div class="info"> <span class="date"><span class="month">';
					echo $row['month'];
					echo '</span> <span class="day">';
					echo $row['day'];
					echo '</span><span class="year">, ';
					echo $row['year'];
					echo '</span></span></div>';
					echo utf8_encode($Parsedown->text($row['text']));
					echo '</article>';
				}
			}
			else 
			{
				echo "<h2>Sorry. Deze pagina bestaat niet.</h2>";
			}
		}
			
	   }
	   else
	   {
	   		?>
            <article class="is-post is-post-excerpt"><header><h2><a href="#">Welkom</a></h2>Welkom op mijn gip website. Mijn naam is Yorick Scheyltjens. Ik ben 17 jaar en woon in Rijkevorsel. Ik studeer informaticabeheer aan het <a href="http://www.immalle.be/immalle/">Immaculata Instituut</a> te Oostmalle. Naast mijn passie met computers en dergelijke, bespeel ik het instrument Trompet in de <a href="http://www.khbroederband.be/">Koninklijke Harmonie Broederband</a> in Rijkevorsel.</br> Mijn website bestaat uit twee delen. Enerzijds de Taken en anderzijds GIP-verslagen. Als u op <a href="http://www.disite.be/yorick.scheyltjens/index.php?id=overzicht">overzicht</a> klikt, verschijnt er een overzicht van alle toegevoegde artikelen, zowel taken als GIP-verslagen Er is ook een overzicht met alleen <a href="http://www.disite.be/yorick.scheyltjens/index.php?id=taken">taken</a> of alleen <a href="http://www.disite.be/yorick.scheyltjens.index.php?id=gip">gipverslagen</a>. Naar deze overzichten kan u ook links in het menu terugvinden.</br></br>
Volgend jaar ga ik voortstuderen. Ik ga beginnen met het betalen van een batchlor diploma Electronica ICT in het <a href="https://www.ap.be/">Artesis Plantijn</a>. Daarna, als ik nog de wil heb om verder te studeren of als ik dan nog geen job heb, ga ik aan een master opleiding Robotica beginnen aan het <a href="https://www.kuleuven.be/kuleuven/">KU Leuven</a>.</br></br>
Electronica ICT is een algemene richting. Ik zal leren programmeren en ik zal netwerkbeheer krijgen. Veel mensen van mijn klas zullen kiezen tussen deze twee. Ook zal ik electronica krijgen. Dit interesseerd mij ook enorm en het is ook een voorbereiding voor Robotica.
Robotica is het  maken van robots.</br></br>
Samen met mijn vader ben ik al websites aan het maken. We hebben een <a href="http://www.disite.be/">eigen bedrijf</a> in het maken van websites en het herstellen van electronische apparaten, vooral computers. We zijn nu bezig met het maken van een systeem waarmee je je evenement online kan zetten en tickets daarvoor online kan verkopen.</br></br>
Ik wens u heel veel leesplezier. Mocht u vragen hebben of iets is niet duidelijk, kan u mij altijd contacteren: Yorick.scheyltjens@immalle.be.
</article>
            
            
            <?
	   
	   }
	  ?>
      
      
    </div>
  </div>
  <div id="sidebar">
    <div id="logo">
      <h1 class="mobileUI-site-name">GIP Yorick</h1>
    </div>
    <nav id="nav" class="mobileUI-site-nav">
      <ul>
        <li><a href="index.php">Home</a></li>
        <li><a href="index.php?id=overzicht">Overzicht</a></li>
        <li><a href="index.php?id=taken">Taken</a></li>
        <li><a href="index.php?id=stage">Stage-verslagen</a></li>
        <li><a href="https://gitlab.com/yorick-scheyltjens/Yorick-Scheyltjens-GIP" target="_blank">Gitlab</a></li>
        <li><a href="https://www.facebook.com/IT-tapa-588405757863108/?fref=ts" target="_blank">Facebook</a></li>
      </ul>
    </nav>
    <div id="copyright">
      <p> &copy; 2015 Yorick Scheyltjens</p>
    </div>
  </div>
</div>
</body>
</html>